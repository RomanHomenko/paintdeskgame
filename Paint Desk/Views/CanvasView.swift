//
//  Canvas.swift
//  Paint Desk
//
//  Created by roman on 7/5/21.
//  Copyright © 2021 roman. All rights reserved.
//

import UIKit

class Canvas: UIView {
    
    // public function
    var lineColor: UIColor = UIColor.black
    var lineWidth: Float = 1
    
    
    func setLineWidth(width: Float) {
        self.lineWidth = width
    }
    
    func setLineColor(color: UIColor) {
        self.lineColor = color
    }
    
    func undo() {
        _ = lines.popLast()
        setNeedsDisplay()
    }
    
    func clear() {
        lines.removeAll()
        setNeedsDisplay()
    }
    
    func randomWord() -> String {
        let arrayOfGameWords: [String] = [
        "Elephant",
        "Cat",
        "Human",
        "Monkey",
        "Telephone",
        "Ipad",
        "Computer",
        "Star",
        "Holy Spirit",
        ]
        
        return arrayOfGameWords.randomElement() ?? "something???"
    }
    
    var lines = [Line]()
    
    override func draw(_ rect: CGRect) {
//        super.draw(rect)
//        super.frame = CGRect(x: 0, y: 0, width: 300, height: 300)
//        super.layer.cornerRadius = 25.0
//        super.clipsToBounds = true
        // custom drawing
        guard let context = UIGraphicsGetCurrentContext() else { return }
        
        // lines on the canvas
        lines.forEach { (line) in
            context.setStrokeColor(line.color.cgColor)
            context.setLineWidth(CGFloat(line.width))
            context.setLineCap(.round)
            for(i, p) in line.points.enumerated() {
                if i == 0 {
                    context.move(to: p)
                } else {
                    context.addLine(to: p)
                }
            }
            context.strokePath()
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        lines.append(Line.init(color: lineColor, width: lineWidth, points: []))
    }
    
    // track the finger as we move across screen(canvas)
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let point = touches.first?.location(in: nil) else { return } // position from x, y on the canvas
        //        print(point)
        
        guard var lastLine = lines.popLast() else { return }
        lastLine.points.append(point)
        lines.append(lastLine)
        
        setNeedsDisplay()
    }
    
}
