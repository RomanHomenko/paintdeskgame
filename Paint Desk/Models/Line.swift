//
//  Line.swift
//  Paint Desk
//
//  Created by roman on 7/7/21.
//  Copyright © 2021 roman. All rights reserved.
//

import UIKit

struct Line {
    let color: UIColor
    var width: Float = 10
    var points: [CGPoint]
}
