//
//  EndScreenViewController.swift
//  Paint Desk
//
//  Created by roman on 7/9/21.
//  Copyright © 2021 roman. All rights reserved.
//

import UIKit

class EndScreenViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func goodbyeButtonTaped(_ sender: Any) {
        exit(0)
    }
    
}
