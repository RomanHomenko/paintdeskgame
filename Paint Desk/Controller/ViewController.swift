//
//  ViewController.swift
//  Paint Desk
//
//  Created by roman on 7/5/21.
//  Copyright © 2021 roman. All rights reserved.
//



/*
 
 To Do List
 
 plans:
 x fix bug with Exit button(add new segue from segueID, not from View id)
 - add new segue from ViewController to WordGuesserVC to copy paint from Canvas in ViewController
 */


import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var whiteButton: UIButton!
    @IBOutlet weak var blackButton: UIButton!
    @IBOutlet weak var redButton: UIButton!
    @IBOutlet weak var wordToGuess: UILabel!
    @IBOutlet weak var undoButton: UIButton!
    @IBOutlet weak var clearButton: UIButton!
    @IBOutlet weak var lineSizeSlider: UISlider!
    @IBOutlet weak var guessTheWordButton: UIButton!
    @IBOutlet weak var refreshWordButton: UIButton!
    @IBOutlet weak var hideWordButton: UIButton!
    
    let canvas = Canvas()
    var saveWordFromHide: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupLayout()
    }
    
    @IBAction func undoButton(_ sender: UIButton) {
        canvas.undo()
    }

    @IBAction func clearButton(_ sender: UIButton) {
        canvas.clear()
    }

    @IBAction func whiteButtonTapped(_ sender: UIButton) {
        canvas.setLineColor(color: whiteButton.backgroundColor ?? .black)
    }
    
    @IBAction func blackButtonTapped(_ sender: UIButton) {
        canvas.setLineColor(color: blackButton.backgroundColor ?? .black)
    }
    
    @IBAction func redButtonTapped(_ sender: Any) {
        canvas.setLineColor(color: redButton.backgroundColor ?? .black)
    }
    
    @IBAction func lineSizeSlider(_ sender: UISlider) {
        canvas.setLineWidth(width: lineSizeSlider.value)
    }
    
    @IBAction func refreshWordButton(_ sender: UIButton) {
        wordToGuess.text! = canvas.randomWord()
        saveWordFromHide = wordToGuess.text!
        canvas.clear()
    }
    
    @IBAction func hideWordButtonTapped(_ sender: UIButton) {
        saveWordFromHide = wordToGuess.text!
        wordToGuess.text! = ""
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destinationVC: WordGuesserViewController = segue.destination as! WordGuesserViewController
        destinationVC.textOfLabel = saveWordFromHide
    }
    
    func setupLayout() {
        canvas.backgroundColor = .white
        wordToGuess.text! = canvas.randomWord()
        whiteButton.backgroundColor = .white
        blackButton.backgroundColor = .black
        redButton.backgroundColor = .red
        canvas.frame = view.frame
        whiteButton.layer.borderWidth = 1
        blackButton.layer.borderWidth = 1
        redButton.layer.borderWidth = 1
        
        view.addSubview(canvas)
        view.addSubview(whiteButton)
        view.addSubview(blackButton)
        view.addSubview(redButton)
        view.addSubview(lineSizeSlider)
        view.addSubview(guessTheWordButton)
        view.addSubview(clearButton)
        view.addSubview(refreshWordButton)
        view.addSubview(undoButton)
        view.addSubview(wordToGuess)
        view.addSubview(hideWordButton)
    }
}

