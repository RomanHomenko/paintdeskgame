//
//  WordGuesserViewController.swift
//  Paint Desk
//
//  Created by roman on 7/9/21.
//  Copyright © 2021 roman. All rights reserved.
//

import UIKit

class WordGuesserViewController: UIViewController {
    @IBOutlet weak var correctOrIncorrectAnswer: UILabel!
    @IBOutlet weak var fieldForUsersAnswer: UITextField!
    @IBOutlet weak var checkButton: UIButton!

    var textOfLabel: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
    }

    @IBAction func checkButtonTaped(_ sender: UIButton) {
        if fieldForUsersAnswer.text == textOfLabel {
            correctOrIncorrectAnswer.text = "You are right!"
        } else {
            correctOrIncorrectAnswer.text = "You are wrong("
        }
    }
    
}
